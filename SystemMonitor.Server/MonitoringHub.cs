﻿using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using SystemMonitor.Domain;
using SystemMonitor.Shared;

namespace SystemMonitor
{
    public class MonitoringHub : Hub
    {
        const string FREQUENCE_UPDATED = "FrequenceUpdated";
        const string RECOURCE_ADDED = "ResourceAdded";
        private readonly ILogger<MonitoringHub> _logger;
        private readonly IServerResourcesService _service;
        private static ConcurrentDictionary<string, ServerRecourcesModel> recources = new ConcurrentDictionary<string, ServerRecourcesModel>();
        public MonitoringHub(ILogger<MonitoringHub> logger, IServerResourcesService service)
        {
            _logger = logger;
            _service = service;
        }

        public async Task UpdateFrecuense(int seconds)
        {
            try
            {
                _service.UpdateFrequense = seconds;
            }
            catch (Exception ex)
            {
                if (ex is ArgumentOutOfRangeException range && range.ParamName == nameof(_service.UpdateFrequense))
                {
                    throw new HubException("Частота обновления должна быть целым числом больше нуля");
                }
                else
                {
                    throw new HubException("Произошла неизвестная ошибка на сервере", ex);
                }
            }
            await Clients.All.SendAsync(FREQUENCE_UPDATED, _service.UpdateFrequense);
        }

        public async Task GetFrecuense()
        {
            await Clients.Caller.SendAsync(FREQUENCE_UPDATED, _service.UpdateFrequense);
        }

        public async Task AddRecources(ServerRecourcesModel model)
        {
            _logger.LogWarning(JsonSerializer.Serialize(model));
            var root = new ServerResources(Guid.NewGuid(), model.Ip, model.RamFreeMegabytes, model.RamTotalMegabytes, model.CpuLoadPercent, DateTime.UtcNow);
            await _service.Add(root,
                model.DriverInfos
                .Select(d => new DiscResource(Guid.NewGuid(), root, d.Name, d.TotalFreeGigabytes, d.TotalGigabytes)));
            recources.AddOrUpdate(model.Ip, model, (_,__) => model);
            await Clients.All.SendAsync(RECOURCE_ADDED, recources.ToArray().Select(r => r.Value).ToArray());
        }
    }
}
