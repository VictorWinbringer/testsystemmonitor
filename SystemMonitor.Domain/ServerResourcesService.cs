﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SystemMonitor.Domain
{

    public sealed class ServerResourcesService : IServerResourcesService
    {
        private readonly IServerResourcesRepository _repository;

        public ServerResourcesService(IServerResourcesRepository repository)
        {
            _repository = repository;
        }

        public int UpdateFrequense
        {
            get => _repository.UpdateFrequense;
            set
            {
                if (value < 1)
                    throw new ArgumentOutOfRangeException(nameof(UpdateFrequense));
                _repository.UpdateFrequense = value;
            }
        }

        public Task Add(ServerResources resources, IEnumerable<DiscResource> discs)
        {
            return _repository.Add(resources, discs);
        }
    }
}
