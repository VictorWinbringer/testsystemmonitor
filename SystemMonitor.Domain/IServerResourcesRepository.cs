﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace SystemMonitor.Domain
{
    public interface IServerResourcesRepository
    {
        Task Add(ServerResources resources, IEnumerable<DiscResource> discs);
        int UpdateFrequense { get; set; }
    }
}
