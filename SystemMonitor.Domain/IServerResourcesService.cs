﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SystemMonitor.Domain
{
    public interface IServerResourcesService
    {
        Task Add(ServerResources resources, IEnumerable<DiscResource> discs);
        int UpdateFrequense { get; set; }
    }
}
