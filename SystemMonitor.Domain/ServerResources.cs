﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SystemMonitor.Domain
{
    public sealed class ServerResources
    {
        public ServerResources(Guid id, string ip, double ramFreeSize, double ramTotalSize, double cpLoad, DateTime created)
        {
            if (id == default)
                throw new ArgumentOutOfRangeException(nameof(Id));
            if (string.IsNullOrWhiteSpace(ip))
                throw new ArgumentNullException(nameof(Ip));
            if (ramFreeSize < 0)
                throw new ArgumentOutOfRangeException(nameof(RamFreeSize));
            if (ramTotalSize < 0)
                throw new ArgumentOutOfRangeException(nameof(RamFreeSize));
            if (cpLoad < 0)
                throw new ArgumentOutOfRangeException(nameof(CpuLoad));
            if (created == default)
                throw new ArgumentOutOfRangeException(nameof(created));
            Id = id;
            Ip = ip;
            RamFreeSize = ramFreeSize;
            RamTotalSize = ramTotalSize;
            CpuLoad = cpLoad;
            Created = created;
        }

        public Guid Id { get; }
        public string Ip { get; }
        public double RamFreeSize { get; }
        public double RamTotalSize { get; }
        public double CpuLoad { get; }
        public DateTime Created { get; }
    }

}
