﻿using System;

namespace SystemMonitor.Domain
{
    public sealed class DiscResource
    {
        public DiscResource(Guid id, ServerResources root, string name, double discFree, double discTotal)
        {
            if (id == default)
                throw new ArgumentOutOfRangeException(nameof(Id));
            if (string.IsNullOrWhiteSpace(name))
                throw new ArgumentNullException(nameof(Name));
            if (discFree < 0)
                throw new ArgumentOutOfRangeException(nameof(DiscFree));
            if (discTotal < 0)
                throw new ArgumentOutOfRangeException(nameof(DiscTotal));
            Id = id;
            Root = root ?? throw new ArgumentNullException(nameof(Root));
            Name = name;
            DiscFree = discFree;
            DiscTotal = discTotal;
        }

        public string Name { get; }
        public double DiscFree { get; }
        public double DiscTotal { get; }
        public Guid Id { get; }
        public ServerResources Root { get; }
    }

}
