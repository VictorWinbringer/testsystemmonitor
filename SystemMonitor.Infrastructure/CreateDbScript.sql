﻿CREATE TABLE IF NOT EXISTS server_resources (
id uuid NOT NULL,
ip varchar(128) NOT NULL,
ram_free double precision NOT NULL,
ram_total double precision NOT NULL,
cpu_load double precision NOT NULL,
created timestamp without time zone NOT NULL,
PRIMARY KEY(id)
);

CREATE TABLE IF NOT EXISTS disc_resources (
id uuid NOT NULL,
name varchar(128) NOT NULL,
root_id uuid NOT NULL,
disc_free double precision NOT NULL,
disc_total double precision NOT NULL,
PRIMARY KEY(id),
FOREIGN KEY (root_id) REFERENCES server_resources (id) ON DELETE CASCADE
);