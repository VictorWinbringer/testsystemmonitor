﻿using Microsoft.Extensions.Caching.Memory;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using SystemMonitor.Domain;

namespace SystemMonitor.Infrastructure
{
    public class ServerResourcesRepository : IServerResourcesRepository
    {
        private readonly NpgsqlConnection _connection;
        private static int _frequence = 1;

        public int UpdateFrequense { get => _frequence; set => Interlocked.Exchange(ref _frequence,value); }

        public ServerResourcesRepository(NpgsqlConnection connection)
        {
            _connection = connection;
        }

        public async Task Add(ServerResources resources, IEnumerable<DiscResource> discs)
        {
            await _connection.OpenAsync();
            using (var tran = _connection.BeginTransaction())
            {
                await InsertResources(resources, tran);
                foreach (var disc in discs)
                {
                    await InsertDiscs(disc, tran);
                }
                await tran.CommitAsync();
            }
        }

        private async Task InsertResources(ServerResources resources, NpgsqlTransaction tran)
        {
            using (var command = new NpgsqlCommand("INSERT INTO server_resources(id,ip,ram_free,ram_total,cpu_load,created) values(@id,@ip,@ram_free,@ram_total,@cpu_load,@created);", _connection, tran))
            {

                command.Parameters.AddWithValue("@id", resources.Id);
                command.Parameters.AddWithValue("@ip", resources.Ip);
                command.Parameters.AddWithValue("@ram_free", resources.RamFreeSize);
                command.Parameters.AddWithValue("@ram_total", resources.RamTotalSize);
                command.Parameters.AddWithValue("@cpu_load", resources.CpuLoad);
                command.Parameters.AddWithValue("@created", resources.Created);
                await command.ExecuteNonQueryAsync();
            }
        }

        private async Task InsertDiscs(DiscResource disc, NpgsqlTransaction tran)
        {
            using (var command = new NpgsqlCommand("INSERT INTO disc_resources(id,root_id,name,disc_free,disc_total) VALUES (@id,@root_id,@name,@disc_free,@disc_total);", _connection, tran))
            {
                command.Parameters.AddWithValue("@id", disc.Id);
                command.Parameters.AddWithValue("@root_id", disc.Root.Id);
                command.Parameters.AddWithValue("@name", disc.Name);
                command.Parameters.AddWithValue("@disc_free", disc.DiscFree);
                command.Parameters.AddWithValue("@disc_total", disc.DiscTotal);
                await command.ExecuteNonQueryAsync();
            }
        }
    }
}
