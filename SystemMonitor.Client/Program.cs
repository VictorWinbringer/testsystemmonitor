﻿using Microsoft.AspNetCore.SignalR.Client;
using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using SystemMonitor.Shared;

namespace SystemMonitor.Client
{
    class Program
    {
        private static int milisecondsToUpdate = 1000;
        static async Task Main(string[] args)
        {
            var drives = DriveInfo.GetDrives();

            var cpuUsage = new PerformanceCounter("Processor", "% Processor Time", "_Total");
            var _ = cpuUsage.NextValue();
            var ramUsage = new MemoryMetricsClient();
            var __ = ramUsage.GetMetrics();


            var connection = new HubConnectionBuilder()
                .WithAutomaticReconnect()
                .WithUrl(new Uri("https://localhost:5001/monitoringHub"))
                .Build();

            connection.On<int>("FrequenceUpdated", (seconds) =>
            {
                Interlocked.Exchange(ref milisecondsToUpdate, seconds * 1000);
            });
            await Connect(connection);
            await connection.SendAsync("GetFrecuense");

            while (true)
            {
                Thread.Sleep(milisecondsToUpdate);

                foreach (var drive in drives)
                {
                    Console.WriteLine($"Name:{drive.Name} Size:{drive.TotalSize} Free: {drive.TotalFreeSpace}");
                }
                var ram = ramUsage.GetMetrics();
                var ip = GetIPAddress();
                var cpu = cpuUsage.NextValue();
                Console.WriteLine($"CPU Usage: {cpu}%");
                Console.WriteLine($"Total Memory:{ram.Total}MB");
                Console.WriteLine($"Avaible Memory: {ram.Free}MB");
                Console.WriteLine($"Ip address{ip}");
                try
                {
                    await connection.SendAsync("AddRecources", new ServerRecourcesModel
                    {
                        Ip = ip,
                        CpuLoadPercent = cpu,
                        DriverInfos = drives
                        .Select(d => new DriverInfoModel { Name = d.Name, TotalFreeGigabytes = d.TotalFreeSpace / 1_000_000_000, TotalGigabytes = d.TotalSize / 1_000_000_000 })
                        .ToList(),
                        RamFreeMegabytes = ram.Free,
                        RamTotalMegabytes = ram.Total
                    });
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    await Connect(connection);
                }
            }
        }

        private static async Task Connect(HubConnection connection)
        {
            while (connection.State == HubConnectionState.Disconnected)
            {
                try
                {
                    await connection.StartAsync();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }

        public static string GetIPAddress()
        {
            var address = string.Empty;
            IPHostEntry Host = default(IPHostEntry);
            string Hostname = null;
            Hostname = System.Environment.MachineName;
            Host = Dns.GetHostEntry(Hostname);
            foreach (IPAddress IP in Host.AddressList)
            {
                if (IP.AddressFamily == AddressFamily.InterNetwork || IP.AddressFamily == AddressFamily.InterNetworkV6)
                {
                    address = Convert.ToString(IP);
                }
            }
            return address;
        }
    }
}
