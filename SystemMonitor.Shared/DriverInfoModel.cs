﻿namespace SystemMonitor.Shared
{
    public sealed class DriverInfoModel
    {
        public string Name { get; set; }
        public long TotalFreeGigabytes { get; set; }
        public long TotalGigabytes { get; set; }
    }
}
