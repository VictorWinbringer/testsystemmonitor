﻿using System;
using System.Collections.Generic;

namespace SystemMonitor.Shared
{
    public sealed class ServerRecourcesModel
    {
        public string Ip { get; set; }
        public double RamFreeMegabytes { get; set; }
        public double RamTotalMegabytes { get; set; }
        public double CpuLoadPercent { get; set; }
      public  List<DriverInfoModel> DriverInfos { get; set; }
    }
}
